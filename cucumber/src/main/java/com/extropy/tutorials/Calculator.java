package com.extropy.tutorials;

/**
 * Hello world!
 */
public class Calculator {
    /**
     * Adds 2 integers
     * @param i
     * @param j
     * @return
     */

    public static int add(int i, int j) {
        return i + j;
    }

    /**
     * Adds 2 doubles
     * @param i
     * @param j
     * @return
     */
    public static double add(double i, double j) {
        return i + j;
    }

    /**
     * Subtract 2nd double from 1st
     * @param i
     * @param j
     * @return
     */
    public static double subtract(double i, double j) {
        return i - j;
    }

    /**
     * Subtract 2nd int from 1st
     * @param i
     * @param j
     * @return
     */
    public static int subtract(int i, int j) {
        return i - j;
    }
}
