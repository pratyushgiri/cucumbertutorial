Feature: Addition of 2 numbers

  Scenario: Addition of 2 positive integers
    Given I am adding two integers
    When I add 2 and 3
    Then I get result 5

  Scenario: Addition of 2 nevative integers
       Given I am adding two integers
       When I add -20 and -30
       Then I get result -50
