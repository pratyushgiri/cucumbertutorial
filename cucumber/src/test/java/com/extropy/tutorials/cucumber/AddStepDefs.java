package com.extropy.tutorials.cucumber;

import com.extropy.tutorials.Calculator;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

/**
 * User: pratyushr
 * Date: 8/27/16
 * Time: 10:06 AM
 */
public class AddStepDefs {

    int sumInt;
    double sumDouble;
    @Given("^I am adding two integers$")
      public void i_am_adding_two_integers() throws Throwable {
          sumInt = 0;
      }

      @When("^I add (-?\\d+) and (-?\\d+)$")
      public void i_add_and(int arg1, int arg2) throws Throwable {
          sumInt = Calculator.add(arg1, arg2);
      }

      @Then("^I get result (-?\\d+)$")
      public void i_get_result(int arg1) throws Throwable {
          assertEquals(sumInt, arg1)  ;
      }



}
