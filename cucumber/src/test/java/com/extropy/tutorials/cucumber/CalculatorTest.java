package com.extropy.tutorials.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * User: pratyushr
 * Date: 8/27/16
 * Time: 9:44 AM
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        features = {"feature"})
public class CalculatorTest {
}
